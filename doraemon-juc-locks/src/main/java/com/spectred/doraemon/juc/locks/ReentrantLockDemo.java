package com.spectred.doraemon.juc.locks;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author spectred
 */
public class ReentrantLockDemo {


    public static void main(String[] args) {
        final ReentrantLock lock = new ReentrantLock();
        for (int i = 0; i < 5; i++) {
            new Thread(new ReentrantLockDemoRunnable(lock)).start();
        }
    }

    static class ReentrantLockDemoRunnable implements Runnable {

        private final ReentrantLock lock;

        public ReentrantLockDemoRunnable(ReentrantLock lock) {
            this.lock = lock;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("🔐" + Thread.currentThread().getName() + ":获得锁");
                System.out.println(">>>> " + lock.toString());
                TimeUnit.SECONDS.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
                System.out.println("🔓" + Thread.currentThread().getName() + ":释放锁");
            }
        }
    }

}
