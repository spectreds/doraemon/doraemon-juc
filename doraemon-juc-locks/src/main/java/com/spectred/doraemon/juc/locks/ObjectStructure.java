package com.spectred.doraemon.juc.locks;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * 对象结构
 *
 * @author spectred
 */
public class ObjectStructure {

    public static void main(String[] args) {
        String details = VM.current().details();
        System.out.println(details);

        Object o = new Object();
        System.out.println(o + ",十六进制哈希: " + Integer.toHexString(o.hashCode()));

        System.out.println();
        String printable = ClassLayout.parseInstance(o).toPrintable();
        System.out.println(printable);
    }
}
