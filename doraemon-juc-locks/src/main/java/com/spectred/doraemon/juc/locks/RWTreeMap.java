package com.spectred.doraemon.juc.locks;

import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author spectred
 */
public class RWTreeMap {


    private final Map<String, String> map = new TreeMap<>();

    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    private final Lock r = rwl.readLock();

    private final Lock w = rwl.writeLock();

    public String get(String key) {
        System.out.println("🔐 READ get Locked by " + Thread.currentThread().getName());
        r.lock();
        try {
            return map.get(key);
        } finally {
            r.unlock();
            System.out.println("🔓 READ get UnLock by " + Thread.currentThread().getName());
        }
    }

    public String put(String key, String value) {
        System.out.println("🔐 WRITE put Locked by " + Thread.currentThread().getName());
        w.lock();
        try {
            return map.put(key, value);
        } finally {
            w.unlock();
            System.out.println("🔓 WRITE put  Unlock by " + Thread.currentThread().getName());
        }
    }

    public void clear() {
        w.lock();
        try {
            map.clear();
        } finally {
            w.unlock();
        }
    }
}
