package com.spectred.doraemon.juc.synchronizer.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * @author spectred
 */
public class Worker implements Runnable {

    private final CountDownLatch startSignal;

    private final CountDownLatch doneSignal;

    public Worker(CountDownLatch startSignal, CountDownLatch doneSignal) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
    }

    @Override
    public void run() {
        try {
            startSignal.await();

            doWork();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            doneSignal.countDown();
        }
    }

    private void doWork() {
        System.out.println(Thread.currentThread().getName());
    }
}
