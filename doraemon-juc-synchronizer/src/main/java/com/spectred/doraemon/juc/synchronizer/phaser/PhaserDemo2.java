package com.spectred.doraemon.juc.synchronizer.phaser;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * @author spectred
 */
public class PhaserDemo2 {

    public static void main(String[] args) throws InterruptedException {
        Phaser phaser = new Phaser(1);

        for (int i = 0; i < 5; i++) {
            phaser.register();
            new Thread(new Task2(phaser)).start();
        }

        TimeUnit.SECONDS.sleep(2L);
        phaser.arriveAndDeregister();
        System.out.println("主线程打开开关");
    }

    static class Task2 implements Runnable {
        private final Phaser phaser;

        public Task2(Phaser phaser) {
            this.phaser = phaser;
        }

        @Override
        public void run() {
            int i = phaser.arriveAndAwaitAdvance();
            System.out.println(Thread.currentThread().getName() + ": 执行完成任务:" + i);
        }
    }
}
