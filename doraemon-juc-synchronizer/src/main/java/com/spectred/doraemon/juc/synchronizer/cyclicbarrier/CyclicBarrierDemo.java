package com.spectred.doraemon.juc.synchronizer.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * CyclicBarrier
 * 让所有的子线程
 * @author spectred
 */
public class CyclicBarrierDemo {

    private static final int N = 10;

    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier barrier = new CyclicBarrier(N, () -> System.out.println("All Done"));

        for (int i = 0; i < N; i++) {
            Thread thread = new Thread(new PrepareWork(barrier));
            thread.start();

            if (i == 3) {
                thread.interrupt();
            }
        }

        Thread.sleep(2000);
        System.out.println("Barrier is broken:" + barrier.isBroken());

    }

    public static class PrepareWork implements Runnable {

        private final CyclicBarrier barrier;

        public PrepareWork(CyclicBarrier barrier) {
            this.barrier = barrier;
        }

        @Override
        public void run() {
            try {
                System.out.println(Thread.currentThread().getName() + ":done");
                barrier.await();
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + "被中断");
            } catch (BrokenBarrierException e) {
                System.out.println(Thread.currentThread().getName() + " throw BrokenBarrierException");
            }
        }
    }
}
