package com.spectred.doraemon.juc.synchronizer.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * @author spectred
 */
public class WorkRunnable implements Runnable {

    private final int i;

    private final CountDownLatch doneSignal;

    public WorkRunnable(int i, CountDownLatch doneSignal) {
        this.i = i;
        this.doneSignal = doneSignal;
    }

    @Override
    public void run() {
        try {
            doWork(i);
        } finally {
            doneSignal.countDown();
        }
    }

    private void doWork(int i) {
        System.out.println(Thread.currentThread() + ": done");
    }
}
