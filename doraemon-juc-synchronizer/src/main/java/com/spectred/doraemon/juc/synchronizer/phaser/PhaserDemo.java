package com.spectred.doraemon.juc.synchronizer.phaser;

import java.util.concurrent.Phaser;

/**
 * @author spectred
 */
public class PhaserDemo {

    public static void main(String[] args) {
        Phaser phaser = new Phaser();

        for (int i = 0; i < 5; i++) {
            phaser.register();

            new Thread(new Task(phaser), "Thread-" + i).start();
        }
    }

    static class Task implements Runnable {

        private final Phaser phaser;

        public Task(Phaser phaser) {
            this.phaser = phaser;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName() + " done. " + phaser.toString());
        }
    }
}
