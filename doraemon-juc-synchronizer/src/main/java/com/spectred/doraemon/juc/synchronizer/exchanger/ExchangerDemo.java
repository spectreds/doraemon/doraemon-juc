package com.spectred.doraemon.juc.synchronizer.exchanger;

import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;

/**
 * @author spectred
 */
public class ExchangerDemo {

    static class FillingLoop implements Runnable {

        private static int data = 0;

        private final Exchanger<Integer> exchanger;

        public FillingLoop(Exchanger<Integer> exchanger) {
            this.exchanger = exchanger;
        }

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                try {
                    data = i;
                    System.out.println(Thread.currentThread().getName() + " 交换前 >>>>:" + data);
                    data = exchanger.exchange(data);
                    System.out.println(Thread.currentThread().getName() + " 交换后 <<<<:" + data);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class EmptyingLoop implements Runnable {

        private static int data = 0;

        private final Exchanger<Integer> exchanger;

        public EmptyingLoop(Exchanger<Integer> exchanger) {
            this.exchanger = exchanger;
        }

        @Override
        public void run() {
            for (; ; ) {
                try {
                    System.out.println(Thread.currentThread().getName() + " 交换前 ---> " + data);
                    TimeUnit.SECONDS.sleep(1L);
                    data = exchanger.exchange(data);
                    System.out.println(Thread.currentThread().getName() + " 交换后 <--- " + data);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Exchanger<Integer> exchanger = new Exchanger<>();
        new Thread(new FillingLoop(exchanger)).start();
        new Thread(new EmptyingLoop(exchanger)).start();

        TimeUnit.SECONDS.sleep(10L);
        System.exit(0);
    }
}
