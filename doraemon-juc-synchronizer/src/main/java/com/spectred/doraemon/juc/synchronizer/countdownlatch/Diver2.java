package com.spectred.doraemon.juc.synchronizer.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * 当所有子线程都执行完,开始执行主线程
 *
 * @author spectred
 */
public class Diver2 {

    private static final int N = 5;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch doneSignal = new CountDownLatch(N);

        for (int i = 0; i < N; ++i) {
            new Thread(new WorkRunnable(i, doneSignal)).start();
        }

        doneSignal.await();
        System.out.println(Thread.currentThread().getName() + " done");
    }
}
