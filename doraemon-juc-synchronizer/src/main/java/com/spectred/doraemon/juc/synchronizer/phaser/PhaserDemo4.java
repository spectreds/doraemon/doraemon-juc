package com.spectred.doraemon.juc.synchronizer.phaser;

import java.util.concurrent.Phaser;

/**
 * @author spectred
 */
public class PhaserDemo4 {

    public static void main(String[] args) {
        int repeats = 3;
        Phaser phaser = new Phaser() {
            @Override
            protected boolean onAdvance(int phase, int registeredParties) {
                System.out.println("---" + phase + "," + registeredParties);
                return phase + 1 >= repeats || registeredParties == 0;
            }
        };

        for (int i = 0; i < 5; i++) {
            phaser.register();
            new Thread(new Task4(phaser)).start();
        }

    }

    static class Task4 implements Runnable {

        private final Phaser phaser;

        public Task4(Phaser phaser) {
            this.phaser = phaser;
        }

        @Override
        public void run() {
            while (!phaser.isTerminated()) {
                int i = phaser.arriveAndAwaitAdvance();
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }
    }
}
