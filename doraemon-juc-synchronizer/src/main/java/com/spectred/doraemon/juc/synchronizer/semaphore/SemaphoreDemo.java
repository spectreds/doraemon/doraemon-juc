package com.spectred.doraemon.juc.synchronizer.semaphore;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * @author spectred
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(3, true);
        for (int i = 0; i < 10; i++) {
            new Thread(new SemaphoreRunnable(semaphore)).start();
        }

    }


    public static class SemaphoreRunnable implements Runnable {

        private final Semaphore semaphore;

        public SemaphoreRunnable(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                System.out.println(Thread.currentThread().getName() + " 获得许可开始执行");
                Thread.sleep(new Random().nextInt(5_000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {

                semaphore.release();
                System.out.println(Thread.currentThread().getName() + " 释放许可,其他线程可执行:" + semaphore.availablePermits());

            }
        }
    }
}
