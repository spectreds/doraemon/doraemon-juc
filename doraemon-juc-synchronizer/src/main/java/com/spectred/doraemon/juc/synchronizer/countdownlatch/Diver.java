package com.spectred.doraemon.juc.synchronizer.countdownlatch;

import java.util.concurrent.CountDownLatch;

/**
 * 当主线程执行完，开始执行子线程
 * @author spectred
 */
public class Diver {

    private static final Integer N = 5;

    public static void main(String[] args) throws InterruptedException {
        CountDownLatch startSignal = new CountDownLatch(1);
        CountDownLatch doneSignal = new CountDownLatch(N);

        for (int i = 0; i < N; i++) {
            new Thread(new Worker(startSignal, doneSignal)).start();
        }

        doSomethingElseA();
        startSignal.countDown();

        doneSignal.await();
        doSomethingElseB();
    }

    private static void doSomethingElseA() {
        System.out.println(Thread.currentThread().getName() + " >>>>");
    }

    private static void doSomethingElseB() {
        System.out.println(Thread.currentThread().getName() + " <<<<");
    }

}
