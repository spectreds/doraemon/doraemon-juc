package com.spectred.doraemon.juc.threads.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/**
 * @author spectred
 */
public class CreateThread {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 1. 继承Thread
        Thread customThread = new CustomThread();
        customThread.start();

        // 2. 实现Runnable
        Thread thread1 = new Thread(new CustomRunnable());
        thread1.start();

        // 3. Callable+FutureTask
        CustomCallable customCallable = new CustomCallable();
        FutureTask<String> futureTask = new FutureTask<>(customCallable);
        new Thread(futureTask).start();
        System.out.println(futureTask.get());


        // 4. ThreadFactory
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        Thread thread2 = threadFactory.newThread(() -> System.out.println(Thread.currentThread().getName()));
        thread2.start();

        // 5. 线程池
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> System.out.println(Thread.currentThread().getName()));
        executorService.shutdown();

        // 6. 线程池+Callable+Future
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        Future<String> submit = scheduledExecutorService.submit(new CustomCallable());
        scheduledExecutorService.shutdown();
        System.out.println(submit.get());


    }
}

class CustomThread extends Thread {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}

class CustomRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}

class CustomCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        return Thread.currentThread().getName();
    }
}
