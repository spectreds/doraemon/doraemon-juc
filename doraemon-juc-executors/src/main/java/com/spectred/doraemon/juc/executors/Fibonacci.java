package com.spectred.doraemon.juc.executors;

import java.util.concurrent.RecursiveTask;

/**
 * @author spectred
 */
public class Fibonacci extends RecursiveTask<Integer> {

    final int n;

    public Fibonacci(int n) {
        this.n = n;
    }

    @Override
    protected Integer compute() {
        if (n <= 1) {
            return n;
        }

        Fibonacci f1 = new Fibonacci(n - 1);
        f1.fork();

        Fibonacci f2 = new Fibonacci(n - 2);

        return f2.compute() + f1.join();
    }

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci(10);
        fibonacci.exec();

        Integer rawResult = fibonacci.getRawResult();
        System.out.println(rawResult);
    }
}
